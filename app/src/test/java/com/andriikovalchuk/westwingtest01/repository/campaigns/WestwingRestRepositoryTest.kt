package com.andriikovalchuk.westwingtest01.repository.campaigns

import com.andriikovalchuk.westwingtest01.domain.model.CampaignResponseTestFactory
import com.andriikovalchuk.westwingtest01.domain.model.Campaigns
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.mockito.Mockito.`when` as whenDo

class WestwingRestRepositoryTest {

    @get:Rule
    val rule: MockitoRule = MockitoJUnit.rule()

    @Mock
    internal lateinit var service: WestwingService
    @InjectMocks
    internal lateinit var westwingRestRepository: WestwingRestRepository

    @Test
    fun shouldGetAllCampaigns() {
        // given
        val entities = CampaignResponseTestFactory.createList(5)
        val dataWeShouldGet = Campaigns(entities)
        whenDo(service.getAllCampaigns()).thenReturn(Single.just(entities))

        // when
        val result = westwingRestRepository.getAllCampaigns().blockingGet()

        // then
        assertEquals(dataWeShouldGet, result)
    }
}