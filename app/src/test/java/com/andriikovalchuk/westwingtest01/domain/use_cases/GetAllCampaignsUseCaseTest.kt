package com.andriikovalchuk.westwingtest01.domain.use_cases

import com.andriikovalchuk.westwingtest01.domain.model.CampaignTestFactory
import com.andriikovalchuk.westwingtest01.domain.repositories.WestwingRepository
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.mockito.Mockito.`when` as whenDo

class GetAllCampaignsUseCaseTest {

    @get:Rule
    val rule: MockitoRule = MockitoJUnit.rule()

    @Mock
    internal lateinit var repository: WestwingRepository
    @InjectMocks
    internal lateinit var getAllCampaignsUseCase: GetAllCampaignsUseCase

    @Test
    fun shouldReturnCorrectData() {
        // given
        val dataWeShouldGet = CampaignTestFactory.createList(5)
        whenDo(repository.getAllCampaigns()).thenReturn(Single.just(dataWeShouldGet))

        // when
        val result = getAllCampaignsUseCase.doWorkOnSameThread().blockingGet()

        // then
        assertEquals(dataWeShouldGet, result)
    }
}