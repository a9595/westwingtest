package com.andriikovalchuk.westwingtest01.presentation.main.di;

import android.content.Context;
import com.andriikovalchuk.westwingtest01.presentation.common.AlertCreator;
import com.andriikovalchuk.westwingtest01.presentation.main.*;
import com.andriikovalchuk.westwingtest01.presentation.navigation.ActivityNavigator;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;

import static org.mockito.Mockito.mock;

@Module
public abstract class TestMainActivityModule {

    @MainActivityScope
    @Provides
    static MainActivityView providesMvpView(
            CampaignsAdapter adapter,
            AlertCreator alertCreator
    ) {
        return new MainActivityView(adapter, alertCreator);
    }

    @MainActivityScope
    @Provides
    static MainActivityModel providesMvpModel() {
        return mock(MainActivityModel.class);
    }

    @MainActivityScope
    @Provides
    static MainActivityPresenter providesMvpPresenter() {
        return mock(MainActivityPresenter.class);
    }

    @MainActivityScope
    @Provides
    static CampaignsAdapter providesAdapter() {
        return mock(CampaignsAdapter.class);
    }

    @MainActivityScope
    @Provides
    static ActivityNavigator providesActivityNavigator() {
        return mock(ActivityNavigator.class);
    }

    @MainActivityScope
    @Provides
    static AlertCreator providesAlertCreator() {
        return mock(AlertCreator.class);
    }

    @MainActivityScope
    @Binds
    abstract Context bindsContext(MainActivity activity);
}
