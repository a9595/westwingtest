package com.andriikovalchuk.westwingtest01.presentation.application.di

import com.andriikovalchuk.westwingtest01.presentation.application.TestApplication
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        TestBuildersModule::class,
        TestUseCaseModule::class,
        TestRepositoryModule::class,
        TestServiceModule::class
    ]
)
interface TestApplicationComponent : AndroidInjector<TestApplication> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<TestApplication>()
}
