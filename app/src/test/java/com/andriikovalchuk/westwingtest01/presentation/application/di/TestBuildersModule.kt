package com.andriikovalchuk.westwingtest01.presentation.application.di

import com.andriikovalchuk.westwingtest01.presentation.main.MainActivity
import com.andriikovalchuk.westwingtest01.presentation.main.di.MainActivityScope
import com.andriikovalchuk.westwingtest01.presentation.main.di.TestMainActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class TestBuildersModule {

    @MainActivityScope
    @ContributesAndroidInjector(modules = [TestMainActivityModule::class])
    abstract fun contributesActivityConstructor(): MainActivity
}