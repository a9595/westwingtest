package com.andriikovalchuk.westwingtest01.presentation.main

import android.net.Uri
import android.os.Bundle
import com.andriikovalchuk.westwingtest01.domain.model.CampaignTestFactory
import com.andriikovalchuk.westwingtest01.presentation.navigation.ActivityNavigator
import io.reactivex.Single
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.robolectric.RobolectricTestRunner
import org.mockito.Mockito.`when` as whenDo


@RunWith(RobolectricTestRunner::class)
class MainActivityPresenterTest {

    @get:Rule
    val rule: MockitoRule = MockitoJUnit.rule()

    @Mock
    internal lateinit var model: MainActivityModel
    @Mock
    internal lateinit var presentedView: MainActivityView
    @Mock
    internal lateinit var navigator: ActivityNavigator
    @InjectMocks
    internal lateinit var presenter: MainActivityPresenter

    @Test
    fun shouldCallDisplayDataIfDataIsValid() {
        // given
        val data = CampaignTestFactory.createList(5)
        whenDo(model.getCampaigns()).thenReturn(
            Single.just(data)
        )

        // when
        bindPresenter()

        // then
        verify(presentedView).displayData(data)
    }

    @Test(expected = IllegalArgumentException::class)
    fun shouldDisplayErrorWhenDataIsNotValid() {
        // given
        val exception = IllegalArgumentException()
        whenDo(model.getCampaigns()).thenThrow(exception)

        // when
        bindPresenter()

        // then
        verify(presentedView).displayError(exception)
    }

    @Test
    fun shouldNavigateToDetails() {
        // given
        val dataItem = CampaignTestFactory.createDataItem()

        // when
        presenter.navigateToDetails(dataItem)

        // then
        verify(navigator).startDetailsActivity(dataItem)
    }

    private fun bindPresenter() {
        presenter.bind(buildCommonBundle(), Bundle(), Uri.EMPTY)
    }

    private fun buildCommonBundle() = Bundle()
}