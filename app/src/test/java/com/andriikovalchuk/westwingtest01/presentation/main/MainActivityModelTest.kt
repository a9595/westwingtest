package com.andriikovalchuk.westwingtest01.presentation.main

import com.andriikovalchuk.westwingtest01.domain.model.CampaignTestFactory
import com.andriikovalchuk.westwingtest01.domain.use_cases.GetAllCampaignsUseCase
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.mockito.Mockito.`when` as whenDo

class MainActivityModelTest {

    @get:Rule
    val rule: MockitoRule = MockitoJUnit.rule()

    @Mock
    internal lateinit var getAllCampaignsUseCase: GetAllCampaignsUseCase
    @InjectMocks
    internal lateinit var mainActivityModel: MainActivityModel

    @Test
    fun shouldGetCampaigns() {
        // given
        val dataWeShouldGet = CampaignTestFactory.createList(5)
        whenDo(getAllCampaignsUseCase.doWork()).thenReturn(Single.just(dataWeShouldGet))

        // when
        val result = mainActivityModel.getCampaigns().blockingGet()

        // then
        verify(getAllCampaignsUseCase).doWork()
        assertEquals(dataWeShouldGet, result)
    }
}