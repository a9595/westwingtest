package com.andriikovalchuk.westwingtest01.presentation.main

import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.GridLayoutManager
import android.view.View
import android.widget.FrameLayout
import base.MvpActivityBaseTest
import com.andriikovalchuk.westwingtest01.R
import com.andriikovalchuk.westwingtest01.anyObject
import com.andriikovalchuk.westwingtest01.domain.model.CampaignTestFactory
import com.andriikovalchuk.westwingtest01.fixedCapture
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner
import java.net.UnknownHostException

@RunWith(RobolectricTestRunner::class)
open class MainActivityViewTest : MvpActivityBaseTest<
        MainActivity, MainActivityPresenter, MainActivityView>() {

    override val testActivityClass: Class<MainActivity>
        get() = MainActivity::class.java

    @Captor
    private lateinit var onRetryArgumentCaptor: ArgumentCaptor<((View) -> Unit)>

    @Before
    override fun setUp() {
        super.setUp()
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun shouldSetupListItemDecoratorOnBound() {
        // given
        val decoration = testedView.list.getItemDecorationAt(0)

        // then
        assertEquals(1, testedView.list.itemDecorationCount)
        assertTrue(decoration is DividerItemDecoration)
    }

    @Test
    fun shouldSetupLayoutManagerOnList() {
        // given
        val layoutManager = testedView.list.layoutManager as GridLayoutManager

        // then
        val expectedSpanCount = getInteger(R.integer.main_list_span_count)
        assertEquals(expectedSpanCount, layoutManager.spanCount)
    }

    @Test
    fun shouldAttachAdapterOnBound() {
        assertNotNull(testedView.list.adapter)
        assertTrue(testedView.list.adapter is CampaignsAdapter)
    }

    @Test
    fun shouldDisplayData() {
        // given
        val data = CampaignTestFactory.createList(5)
        val campaignsAdapter = testedView.list.adapter as CampaignsAdapter

        // when
        testedView.displayData(data)

        // then

        verify(campaignsAdapter, times(1)).setData(data.dataItems)
    }

    @Test
    fun shouldDisplayErrorWhenUnknownErrorOccurred() {
        // given
        val errorCodeOtherThanUnknownHost = Throwable()
        val alertCreator = testedView.alertCreator

        // when
        testedView.displayError(errorCodeOtherThanUnknownHost)

        // then
        verify(
            alertCreator,
            times(1)
        ).showUnknownErrorToast()
        verifyNoMoreInteractions(alertCreator)
    }

    @Test
    fun shouldNoInternetIfUnknownHostException() {
        // given
        val noInternetError = UnknownHostException()
        val parentView = testedView.root

        // when
        testedView.displayError(noInternetError)

        // then
        verify(
            testedView.alertCreator,
            times(1)
        ).showNoInternetSnackBar(
            anyObject<FrameLayout>(),
            onRetryArgumentCaptor.fixedCapture()
        )

        // when
        onRetryArgumentCaptor.value.invoke(parentView)

        // then
        verify(testedView.presenter).fetchData()
    }
}