package com.andriikovalchuk.westwingtest01.presentation.application.di

import com.andriikovalchuk.westwingtest01.domain.repositories.WestwingRepository
import com.andriikovalchuk.westwingtest01.domain.use_cases.GetAllCampaignsUseCase
import dagger.Module
import dagger.Provides
import org.mockito.Mockito.mock

@Module
internal class TestUseCaseModule {

    @Provides
    internal fun providesGetRepositoriesAndUsersUseCase(githubRepository: WestwingRepository): GetAllCampaignsUseCase =
        mock(GetAllCampaignsUseCase::class.java)
}