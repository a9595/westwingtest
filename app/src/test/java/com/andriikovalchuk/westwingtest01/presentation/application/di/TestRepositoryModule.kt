package com.andriikovalchuk.westwingtest01.presentation.application.di

import com.andriikovalchuk.westwingtest01.domain.repositories.WestwingRepository
import com.andriikovalchuk.westwingtest01.repository.campaigns.WestwingRestRepository
import com.andriikovalchuk.westwingtest01.repository.campaigns.WestwingService
import dagger.Module
import dagger.Provides
import org.mockito.Mockito.mock
import javax.inject.Singleton

@Module
class TestRepositoryModule {

    @Provides
    @Singleton
    internal fun providesRestRepository(service: WestwingService): WestwingRepository {
        return mock(WestwingRestRepository::class.java)
    }
}
