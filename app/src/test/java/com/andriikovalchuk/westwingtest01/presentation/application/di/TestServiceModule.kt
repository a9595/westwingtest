package com.andriikovalchuk.westwingtest01.presentation.application.di

import com.andriikovalchuk.westwingtest01.repository.campaigns.WestwingService
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import org.mockito.Mockito.mock

@Module
class TestServiceModule {

    @Provides
    internal fun provideGson() = mock(Gson::class.java)

    @Provides
    internal fun providesService(okHttpClient: OkHttpClient, gson: Gson) =
        mock(WestwingService::class.java)

    @Provides
    internal fun provideOkHttpClient(okHttpCache: Cache) =
        mock(OkHttpClient::class.java)

    @Provides
    internal fun provideOkHttpCache() = mock(Cache::class.java)
}
