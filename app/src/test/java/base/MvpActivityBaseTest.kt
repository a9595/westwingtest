package base

import android.content.Intent
import com.andriikovalchuk.westwingtest01.presentation.base.MvpActivity
import com.andriikovalchuk.westwingtest01.presentation.base.PresentedActivityView
import com.andriikovalchuk.westwingtest01.presentation.base.Presenter
import org.junit.After
import org.junit.Before
import org.robolectric.Robolectric
import org.robolectric.RuntimeEnvironment
import org.robolectric.android.controller.ActivityController

abstract class MvpActivityBaseTest<
        T : MvpActivity<PresenterType, PresentedViewType>,
        PresenterType : Presenter<PresentedViewType>,
        out PresentedViewType : PresentedActivityView<PresenterType>> {

    lateinit var testedActivity: T
        private set

    private lateinit var activityController: ActivityController<T>
    protected abstract val testActivityClass: Class<T>

    val testedPresenter: PresenterType
        get() = testedActivity.presenter
    val testedView: PresentedViewType
        get() = testedActivity.presentedView

    @Before
    open fun setUp() {
        activityController = Robolectric.buildActivity(testActivityClass, activityIntent)
        testedActivity = activityController.get()
        activityController.create().visible()
    }

    @After
    fun tearDown() {
        activityController.destroy()
    }

    protected open val activityIntent: Intent
        get() = Intent(RuntimeEnvironment.application, testActivityClass)

    protected fun getInteger(resourceId: Int): Int {
        return RuntimeEnvironment.application.resources.getInteger(resourceId)
    }
}