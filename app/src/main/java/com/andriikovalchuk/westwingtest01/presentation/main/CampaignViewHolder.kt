package com.andriikovalchuk.westwingtest01.presentation.main

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.andriikovalchuk.westwingtest01.R
import com.andriikovalchuk.westwingtest01.domain.model.CampaignDataItem
import com.andriikovalchuk.westwingtest01.presentation.base.adapter.BaseRecyclerViewHolder
import com.andriikovalchuk.westwingtest01.presentation.base.bindView
import com.andriikovalchuk.westwingtest01.presentation.base.loadImageWithPlaceholder

open class CampaignViewHolder(val view: View) : BaseRecyclerViewHolder<CampaignDataItem>(view) {

    var onItemClicked: (CampaignDataItem) -> Unit = {}
    private val campaignName: TextView by bindView(R.id.campaignName)
    private val picture: ImageView by bindView(R.id.picture)
    private val campaignItemRoot: View by bindView(R.id.campaignItemRoot)

    override fun displayItem(item: CampaignDataItem) {
        picture.loadImageWithPlaceholder(item.imageUrl, R.mipmap.ic_launcher)
        campaignName.text = item.name
        campaignItemRoot.setOnClickListener { onItemClicked(item) }
    }

}
