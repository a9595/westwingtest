package com.andriikovalchuk.westwingtest01.presentation.base

import io.reactivex.disposables.Disposable

interface DisposableRegistrants {
    fun registerDisposable(disposable: Disposable)
}
