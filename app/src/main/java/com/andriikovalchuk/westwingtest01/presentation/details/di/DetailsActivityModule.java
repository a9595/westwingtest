package com.andriikovalchuk.westwingtest01.presentation.details.di;

import android.support.v7.app.AppCompatActivity;
import com.andriikovalchuk.westwingtest01.presentation.details.DetailsActivity;
import com.andriikovalchuk.westwingtest01.presentation.details.DetailsActivityPresenter;
import com.andriikovalchuk.westwingtest01.presentation.details.DetailsActivityView;
import com.tbruyelle.rxpermissions2.RxPermissions;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
public abstract class DetailsActivityModule {

    @DetailsActivityScope
    @Provides
    static DetailsActivityView providesMvpView() {
        return new DetailsActivityView();
    }

    @DetailsActivityScope
    @Provides
    static DetailsActivityPresenter providesMvpPresenter(
            DetailsActivityView view,
            RxPermissions rxPermissions
    ) {
        return new DetailsActivityPresenter(rxPermissions, view);
    }

    @DetailsActivityScope
    @Provides
    static RxPermissions providesRxPermissions(AppCompatActivity context) {
        return new RxPermissions(context);
    }

    @DetailsActivityScope
    @Binds
    abstract AppCompatActivity bindsContext(DetailsActivity activity);
}
