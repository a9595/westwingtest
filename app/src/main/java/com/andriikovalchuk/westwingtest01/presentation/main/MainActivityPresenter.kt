package com.andriikovalchuk.westwingtest01.presentation.main

import android.net.Uri
import android.os.Bundle
import com.andriikovalchuk.westwingtest01.domain.model.CampaignDataItem
import com.andriikovalchuk.westwingtest01.presentation.base.Presenter
import com.andriikovalchuk.westwingtest01.presentation.base.register
import com.andriikovalchuk.westwingtest01.presentation.navigation.ActivityNavigator
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

open class MainActivityPresenter @Inject constructor(
    private val model: MainActivityModel,
    private val navigator: ActivityNavigator,
    view: MainActivityView
) : Presenter<MainActivityView>(view) {

    override fun bind(intentBundle: Bundle, savedInstanceState: Bundle, intentData: Uri?) {
        fetchData()
    }

    open fun fetchData() {
        model.getCampaigns()
            .subscribeBy(
                onSuccess = { presentedView.displayData(it) },
                onError = { presentedView.displayError(it) }

            ).register(this)
    }

    open fun navigateToDetails(item: CampaignDataItem) {
        navigator.startDetailsActivity(item)
    }
}
