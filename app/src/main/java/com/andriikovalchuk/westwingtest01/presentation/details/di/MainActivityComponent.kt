package com.andriikovalchuk.westwingtest01.presentation.details.di

import com.andriikovalchuk.westwingtest01.presentation.details.DetailsActivity
import dagger.Subcomponent
import dagger.android.AndroidInjector

@DetailsActivityScope
@Subcomponent
interface DetailsActivityComponent : AndroidInjector<DetailsActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<DetailsActivity>()
}
