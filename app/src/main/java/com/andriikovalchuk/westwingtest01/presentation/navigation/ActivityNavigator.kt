package com.andriikovalchuk.westwingtest01.presentation.navigation

import android.content.Context
import com.andriikovalchuk.westwingtest01.domain.model.CampaignDataItem
import com.andriikovalchuk.westwingtest01.presentation.details.DetailsActivity

open class ActivityNavigator(private val context: Context) {

    open fun startDetailsActivity(item: CampaignDataItem) {
        val intent = DetailsActivity.getIntent(context, item)
        context.startActivity(intent)
    }
}
