package com.andriikovalchuk.westwingtest01.presentation.main

import android.graphics.drawable.Drawable
import android.support.annotation.LayoutRes
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.FrameLayout
import com.andriikovalchuk.westwingtest01.R
import com.andriikovalchuk.westwingtest01.domain.model.Campaigns
import com.andriikovalchuk.westwingtest01.presentation.base.PresentedActivityView
import com.andriikovalchuk.westwingtest01.presentation.common.AlertCreator
import java.net.UnknownHostException


open class MainActivityView(
    private val adapter: CampaignsAdapter,
    val alertCreator: AlertCreator
) : PresentedActivityView<MainActivityPresenter>() {

    @LayoutRes
    override val layoutResId = R.layout.activity_main

    private val listDividerDrawable: Drawable by bindDrawable(R.drawable.main_list_divider)
    val root: FrameLayout by bindView(R.id.root)
    val list: RecyclerView by bindView(R.id.list)

    override fun onViewsBound() {
        setupListItemDecorator()
        setupLayoutManager()
        attachAdapter()
    }

    private fun setupListItemDecorator() {
        val itemDecorator = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        itemDecorator.setDrawable(listDividerDrawable)
        list.addItemDecoration(itemDecorator)
    }

    private fun setupLayoutManager() {
        val columns = context.resources.getInteger(R.integer.main_list_span_count)
        val gridLayoutManager = GridLayoutManager(context, columns)
        list.layoutManager = gridLayoutManager
    }

    private fun attachAdapter() {
        adapter.onItemClicked = { presenter.navigateToDetails(it) }
        list.adapter = adapter
    }

    open fun displayData(data: Campaigns) {
        adapter.setData(data.dataItems)
        Log.d("", data.dataItems.toString())
    }

    open fun displayError(throwable: Throwable) {
        if (throwable is UnknownHostException) {
            alertCreator.showNoInternetSnackBar(root) {
                presenter.fetchData()
            }
        } else {
            alertCreator.showUnknownErrorToast()
        }
    }
}
