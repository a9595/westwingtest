package com.andriikovalchuk.westwingtest01.presentation.main

import android.view.View
import com.andriikovalchuk.westwingtest01.R
import com.andriikovalchuk.westwingtest01.domain.model.CampaignDataItem
import com.andriikovalchuk.westwingtest01.presentation.base.adapter.BaseRecyclerAdapter

open class CampaignsAdapter : BaseRecyclerAdapter<CampaignDataItem>() {

    var onItemClicked: (CampaignDataItem) -> Unit = {}

    override fun layoutId(viewType: Int) = R.layout.item_campaign

    override fun createViewHolder(view: View, viewType: Int) = CampaignViewHolder(view).apply {
        onItemClicked = this@CampaignsAdapter.onItemClicked
    }
}