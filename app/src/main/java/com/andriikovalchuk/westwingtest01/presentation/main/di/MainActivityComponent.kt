package com.andriikovalchuk.westwingtest01.presentation.main.di

import com.andriikovalchuk.westwingtest01.presentation.main.MainActivity
import dagger.Subcomponent
import dagger.android.AndroidInjector

@MainActivityScope
@Subcomponent
interface MainActivityComponent : AndroidInjector<MainActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<MainActivity>()
}
