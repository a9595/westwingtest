package com.andriikovalchuk.westwingtest01.presentation.application.di

import com.andriikovalchuk.westwingtest01.presentation.application.WestWingApplication
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        BuildersModule::class,
        ApplicationModule::class,
        UseCaseModule::class,
        RepositoryModule::class,
        ServiceModule::class
    ]
)
interface WestWingAppComponent: AndroidInjector<WestWingApplication> {

    @Component.Builder
    abstract class Builder: AndroidInjector.Builder<WestWingApplication>()
}
