package com.andriikovalchuk.westwingtest01.presentation.details.di;

import javax.inject.Scope;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@Scope
public @interface DetailsActivityScope {
}
