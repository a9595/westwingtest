package com.andriikovalchuk.westwingtest01.presentation.base.adapter

import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import java.util.*

abstract class BaseRecyclerAdapter<T : Any> : RecyclerView.Adapter<BaseRecyclerViewHolder<T>>() {

    internal open val data: ArrayList<T> = ArrayList()
    @LayoutRes abstract fun layoutId(viewType: Int): Int

    override fun getItemCount() = data.size

    abstract fun createViewHolder(view: View, viewType: Int): BaseRecyclerViewHolder<T>

    override fun onBindViewHolder(holder: BaseRecyclerViewHolder<T>, position: Int) {
        holder.displayItem(data[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseRecyclerViewHolder<T> {
        return createViewHolder(
                LayoutInflater.from(parent.context).inflate(layoutId(viewType), parent, false),
                viewType
        )
    }

    open fun setData(newData: List<T>) {
        if (data != newData) {
            data.clear()
            data.addAll(newData)
            notifyDataSetChanged()
        }
    }
}
