package com.andriikovalchuk.westwingtest01.presentation.base

import android.support.annotation.DrawableRes
import android.widget.ImageView
import com.squareup.picasso.Picasso

fun ImageView.loadImageWithPlaceholder(imagePath: String, @DrawableRes placeholderResId: Int) {
    if (imagePath.isBlank()) {
        setImageResource(placeholderResId)
    } else {
        Picasso.get()
            .load(imagePath)
            .placeholder(placeholderResId)
            .error(placeholderResId)
            .into(this)
    }
}