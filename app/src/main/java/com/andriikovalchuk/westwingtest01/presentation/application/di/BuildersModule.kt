package com.andriikovalchuk.westwingtest01.presentation.application.di

import com.andriikovalchuk.westwingtest01.presentation.details.DetailsActivity
import com.andriikovalchuk.westwingtest01.presentation.details.di.DetailsActivityModule
import com.andriikovalchuk.westwingtest01.presentation.details.di.DetailsActivityScope
import com.andriikovalchuk.westwingtest01.presentation.main.MainActivity
import com.andriikovalchuk.westwingtest01.presentation.main.di.MainActivityModule
import com.andriikovalchuk.westwingtest01.presentation.main.di.MainActivityScope
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BuildersModule {

    @MainActivityScope
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun contributesMainActivityConstructor(): MainActivity


    @DetailsActivityScope
    @ContributesAndroidInjector(modules = [DetailsActivityModule::class])
    abstract fun contributesDetailsActivityConstructor(): DetailsActivity
}