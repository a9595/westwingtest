package com.andriikovalchuk.westwingtest01.presentation.details

import android.content.Context
import android.content.Intent
import com.andriikovalchuk.westwingtest01.domain.model.CampaignDataItem
import com.andriikovalchuk.westwingtest01.presentation.base.MvpActivity
import dagger.android.AndroidInjection
import javax.inject.Inject

class DetailsActivity : MvpActivity<DetailsActivityPresenter, DetailsActivityView>() {

    @Inject lateinit var detailsActivityPresenter: DetailsActivityPresenter
    @Inject lateinit var detailsActivityView: DetailsActivityView

    override val presenter: DetailsActivityPresenter get() = detailsActivityPresenter
    override val presentedView: DetailsActivityView get() = detailsActivityView

    override fun injectIntoGraph() {
        AndroidInjection.inject(this)
    }

    companion object {
        const val EXTRA_KEY_CAMPAIGN = "extra_campaign"

        fun getIntent(
                context: Context,
                item: CampaignDataItem
        ): Intent {
            return Intent(context, DetailsActivity::class.java).apply {
                putExtra(EXTRA_KEY_CAMPAIGN, item)
            }
        }
    }
}
