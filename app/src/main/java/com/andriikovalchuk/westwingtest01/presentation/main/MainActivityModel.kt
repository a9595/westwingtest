package com.andriikovalchuk.westwingtest01.presentation.main

import com.andriikovalchuk.westwingtest01.domain.use_cases.GetAllCampaignsUseCase
import javax.inject.Inject

open class MainActivityModel @Inject constructor(
    private val getAllCampaignsUseCase: GetAllCampaignsUseCase
) {

    open fun getCampaigns() = getAllCampaignsUseCase.doWork()
}