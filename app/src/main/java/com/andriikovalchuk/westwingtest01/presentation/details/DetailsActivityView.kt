package com.andriikovalchuk.westwingtest01.presentation.details

import android.support.annotation.LayoutRes
import android.support.design.widget.FloatingActionButton
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.TextView
import com.andriikovalchuk.westwingtest01.R
import com.andriikovalchuk.westwingtest01.domain.model.CampaignDataItem
import com.andriikovalchuk.westwingtest01.presentation.base.PresentedActivityView
import com.andriikovalchuk.westwingtest01.presentation.base.loadImageWithPlaceholder
import com.flaviofaria.kenburnsview.KenBurnsView
import com.flaviofaria.kenburnsview.RandomTransitionGenerator


class DetailsActivityView(
) : PresentedActivityView<DetailsActivityPresenter>() {

    @LayoutRes
    override val layoutResId = R.layout.activity_details
    private val picture: KenBurnsView by bindView(R.id.picture)
    private val title: TextView by bindView(R.id.title)
    private val description: TextView by bindView(R.id.description)
    private val callNowFab: FloatingActionButton by bindView(R.id.callNowFab)

    override fun onViewsBound() {
        setupKenBurnsEffect()
        callNowFab.setOnClickListener { presenter.callWithPermissionCheck() }
    }

    private fun setupKenBurnsEffect() {
        val generator = RandomTransitionGenerator(TRANSITION_DURATION, AccelerateDecelerateInterpolator())
        picture.setTransitionGenerator(generator)
    }

    fun displayData(item: CampaignDataItem) {
        picture.loadImageWithPlaceholder(item.imageUrl, R.mipmap.ic_launcher)
        title.text = item.name
        description.text = item.description
    }

    companion object {
        private const val TRANSITION_DURATION = 5000L
    }
}
