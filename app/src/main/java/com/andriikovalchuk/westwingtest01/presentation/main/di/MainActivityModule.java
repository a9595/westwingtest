package com.andriikovalchuk.westwingtest01.presentation.main.di;

import android.content.Context;
import com.andriikovalchuk.westwingtest01.domain.use_cases.GetAllCampaignsUseCase;
import com.andriikovalchuk.westwingtest01.presentation.common.AlertCreator;
import com.andriikovalchuk.westwingtest01.presentation.main.CampaignsAdapter;
import com.andriikovalchuk.westwingtest01.presentation.main.MainActivityModel;
import com.andriikovalchuk.westwingtest01.presentation.main.MainActivityPresenter;
import com.andriikovalchuk.westwingtest01.presentation.main.MainActivityView;
import com.andriikovalchuk.westwingtest01.presentation.navigation.ActivityNavigator;
import dagger.Module;
import dagger.Provides;

@Module
public abstract class MainActivityModule {

    @MainActivityScope
    @Provides
    static MainActivityView providesMvpView(
            CampaignsAdapter adapter,
            AlertCreator alertCreator
    ) {
        return new MainActivityView(adapter, alertCreator);
    }

    @MainActivityScope
    @Provides
    static MainActivityModel providesMvpModel(GetAllCampaignsUseCase useCase) {
        return new MainActivityModel(useCase);
    }

    @MainActivityScope
    @Provides
    static MainActivityPresenter providesMvpPresenter(
            MainActivityModel model,
            MainActivityView view,
            ActivityNavigator navigator
    ) {
        return new MainActivityPresenter(model, navigator, view);
    }

    @MainActivityScope
    @Provides
    static ActivityNavigator providesActivityNavigator(Context context) {
        return new ActivityNavigator(context);
    }

    @MainActivityScope
    @Provides
    static AlertCreator providesAlertCreator(Context context) {
        return new AlertCreator(context);
    }

    @MainActivityScope
    @Provides
    static CampaignsAdapter bindsAdapter() {
        return new CampaignsAdapter();
    }
}
