package com.andriikovalchuk.westwingtest01.presentation.main.di;

import javax.inject.Scope;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@Scope
public @interface MainActivityScope {
}
