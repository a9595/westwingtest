package com.andriikovalchuk.westwingtest01.presentation.base.adapter

import android.support.v7.widget.RecyclerView
import android.view.View

abstract class BaseRecyclerViewHolder<in T : Any>(view: View) : RecyclerView.ViewHolder(view) {

    abstract fun displayItem(item: T)
}