package com.andriikovalchuk.westwingtest01.presentation.application.di

import android.content.Context
import com.andriikovalchuk.westwingtest01.repository.campaigns.WestwingService
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

import java.io.File
import java.util.concurrent.TimeUnit

@Module
class ServiceModule {

    @Provides
    internal fun provideGson(): Gson {
        return GsonBuilder()
            .create()
    }

    @Provides
    internal fun providesService(okHttpClient: OkHttpClient, gson: Gson): WestwingService {
        val retrofit = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
        return retrofit.create(WestwingService::class.java)
    }

    @Provides
    internal fun provideOkHttpClient(okHttpCache: Cache): OkHttpClient {
        return OkHttpClient()
            .newBuilder()
            .readTimeout(HTTP_READ_WRITE_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .connectTimeout(HTTP_READ_WRITE_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .cache(okHttpCache)
            .build()
    }

    @Provides
    internal fun provideOkHttpCache(context: Context): Cache {
        val cacheFile = File(context.applicationContext.cacheDir, HTTP_CACHE_DIR_NAME)
        if (!cacheFile.exists()) {

            cacheFile.mkdirs()
        }
        return Cache(cacheFile, HTTP_CACHE_SIZE.toLong())
    }

    companion object {

        private const val HTTP_CACHE_DIR_NAME = "http-cache"
        private const val HTTP_CACHE_SIZE = 10 * 1024 * 1024
        private const val HTTP_READ_WRITE_TIMEOUT = 30
        const val BASE_URL = "https://static.westwing.de/"
    }
}
