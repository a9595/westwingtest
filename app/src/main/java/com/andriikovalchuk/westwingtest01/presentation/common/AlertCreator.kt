package com.andriikovalchuk.westwingtest01.presentation.common

import android.content.Context
import android.support.design.widget.Snackbar
import android.view.View
import android.widget.Toast
import com.andriikovalchuk.westwingtest01.R

open class AlertCreator(private val context: Context) {

    private var currentlyDisplayedSnackBar: Snackbar? = null

    open fun showNoInternetSnackBar(
        parentView: View,
        onRetryClick: ((View) -> Unit)
    ) {
        currentlyDisplayedSnackBar = createThenShowNewActionErrorSnackBar(parentView, onRetryClick)
    }

    open fun showUnknownErrorToast() {
        Toast.makeText(context, R.string.unknown_error, Toast.LENGTH_LONG).show()
    }

    private fun createThenShowNewActionErrorSnackBar(
        parentView: View,
        onRetryClick: ((View) -> Unit)?
    ): Snackbar {
        return Snackbar.make(parentView, R.string.no_internet, Snackbar.LENGTH_INDEFINITE).apply {
            setAction(R.string.retry, onRetryClick)
            show()
        }
    }

}