package com.andriikovalchuk.westwingtest01.presentation.application.di

import com.andriikovalchuk.westwingtest01.domain.repositories.WestwingRepository
import com.andriikovalchuk.westwingtest01.domain.use_cases.GetAllCampaignsUseCase
import dagger.Module
import dagger.Provides

@Module
internal class UseCaseModule {

    @Provides
    fun providesGetAllCampaignsUseCase(
        repository: WestwingRepository
    ): GetAllCampaignsUseCase {
        return GetAllCampaignsUseCase(repository)
    }
}