package com.andriikovalchuk.westwingtest01.presentation.details

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import com.andriikovalchuk.westwingtest01.R
import com.andriikovalchuk.westwingtest01.domain.model.CampaignDataItem
import com.andriikovalchuk.westwingtest01.presentation.base.Presenter
import com.andriikovalchuk.westwingtest01.presentation.base.register
import com.andriikovalchuk.westwingtest01.presentation.details.DetailsActivity.Companion.EXTRA_KEY_CAMPAIGN
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject


class DetailsActivityPresenter @Inject constructor(
    private val rxPermissions: RxPermissions, // TODO remove
    view: DetailsActivityView
) : Presenter<DetailsActivityView>(view) {

    private lateinit var item: CampaignDataItem

    override fun bind(intentBundle: Bundle, savedInstanceState: Bundle, intentData: Uri?) {
        extractData(intentBundle)
        presentedView.displayData(item)
    }

    private fun extractData(intentBundle: Bundle) {
        item = intentBundle.getParcelable(EXTRA_KEY_CAMPAIGN)
                ?: throw IllegalArgumentException("$EXTRA_KEY_CAMPAIGN can't be null")
    }

    fun callWithPermissionCheck() {
        rxPermissions.request(Manifest.permission.CALL_PHONE)
            .subscribeBy(
                onNext = { granted ->
                    if (granted) callSupport()
                    else permissionNotGranted()
                }
            ).register(this)
    }

    private fun callSupport() {
        val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts(PHONE_SCHEME, SUPPORT_PHONE_NUMBER, null))
        context.startActivity(intent)
    }

    private fun permissionNotGranted() =
        Toast.makeText(context, context.getString(R.string.permission_needed), Toast.LENGTH_SHORT).show()

    companion object {
        const val PHONE_SCHEME = "tel"
        const val SUPPORT_PHONE_NUMBER = "08008767001"
    }
}
