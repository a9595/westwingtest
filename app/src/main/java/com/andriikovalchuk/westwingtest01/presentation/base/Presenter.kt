package com.andriikovalchuk.westwingtest01.presentation.base

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.annotation.VisibleForTesting
import android.view.MenuItem
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class Presenter<out PresentedViewType : PresentedView<*, *>>(
    val presentedView: PresentedViewType
) : DisposableRegistrants {

    protected val context: Context
        get() = presentedView.context

    val tag: String
        get() = javaClass.simpleName

    val compositeDisposable = CompositeDisposable()
        @VisibleForTesting get() = field

    abstract fun bind(intentBundle: Bundle, savedInstanceState: Bundle, intentData: Uri?)

    @CallSuper
    open fun unbind() {
        compositeDisposable.dispose()
    }

    open fun onResume() {
        /* NO OP */
    }

    open fun onPause() {
        /* NO OP */
    }

    open fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        /* NO OP */
    }

    open fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        /* NO OP */
    }

    open fun onBackPressed(): Boolean {
        return false
    }

    open fun saveInstanceState(outState: Bundle) {
        /* NO OP */
    }

    open fun onContextItemSelected(item: MenuItem?): Boolean {
        return false
    }

    override fun registerDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }
}
