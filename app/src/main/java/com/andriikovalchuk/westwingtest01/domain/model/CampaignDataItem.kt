package com.andriikovalchuk.westwingtest01.domain.model

import android.os.Parcel
import android.os.Parcelable
import com.andriikovalchuk.westwingtest01.repository.campaigns.model.DataItemResponse

data class CampaignDataItem(
        val imageUrl: String,
        val name: String,
        val description: String,
        val urlKey: String
) : Parcelable {
    constructor(item: DataItemResponse?) : this(
            item?.image?.url.orEmpty(),
            item?.name.orEmpty(),
            item?.description.orEmpty(),
            item?.urlKey.orEmpty()
    )

    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(imageUrl)
        writeString(name)
        writeString(description)
        writeString(urlKey)
    }

    companion object {
        @JvmField val CREATOR: Parcelable.Creator<CampaignDataItem> = object : Parcelable.Creator<CampaignDataItem> {
            override fun createFromParcel(source: Parcel): CampaignDataItem = CampaignDataItem(source)
            override fun newArray(size: Int): Array<CampaignDataItem?> = arrayOfNulls(size)
        }
    }
}
