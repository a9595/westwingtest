package com.andriikovalchuk.westwingtest01.domain.model

object CampaignTestFactory {

    fun createList(count: Int): Campaigns {
        val list = (0..count).map {
            CampaignDataItem(
                "imageUrl $it",
                "name $it",
                "description $it",
                "urlKey $it"
            )
        }

        return Campaigns(list)
    }

    fun createDataItem() = CampaignDataItem(
        "imageUrl",
        "name",
        "description",
        "urlKey"
    )
}