package com.andriikovalchuk.westwingtest01.domain.use_cases

import com.andriikovalchuk.westwingtest01.domain.model.Campaigns
import com.andriikovalchuk.westwingtest01.domain.repositories.WestwingRepository
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

open class GetAllCampaignsUseCase(private val repository: WestwingRepository) {

    open fun doWork(): Single<Campaigns> = perform()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

    open fun doWorkOnSameThread(): Single<Campaigns> = perform()

    private fun perform() = repository
            .getAllCampaigns()

}
