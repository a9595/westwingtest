package com.andriikovalchuk.westwingtest01.domain.model

import com.andriikovalchuk.westwingtest01.repository.campaigns.model.CampaignsResponseEntity
import com.andriikovalchuk.westwingtest01.repository.campaigns.model.DataItemResponse
import com.andriikovalchuk.westwingtest01.repository.campaigns.model.ImageResponse
import com.andriikovalchuk.westwingtest01.repository.campaigns.model.MetadataResponse

object CampaignResponseTestFactory {

    fun createList(count: Int): CampaignsResponseEntity {
        val list = (0..count).map {
            DataItemResponse(
                image = ImageResponse("url"),
                name = "name",
                urlKey = "key",
                description = "desc"
            )
        }

        val response = MetadataResponse(list, list.size)
        return CampaignsResponseEntity(metadata = response)
    }
}