package com.andriikovalchuk.westwingtest01.domain.model

import com.andriikovalchuk.westwingtest01.repository.campaigns.model.CampaignsResponseEntity

data class Campaigns(val dataItems: List<CampaignDataItem>) {

    constructor(item: CampaignsResponseEntity) : this(
        item.metadata?.data?.map { CampaignDataItem(it) }
            ?: throw IllegalArgumentException("list can't be null"))
}
