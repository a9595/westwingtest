package com.andriikovalchuk.westwingtest01.domain.repositories

import com.andriikovalchuk.westwingtest01.domain.model.Campaigns
import io.reactivex.Single

interface WestwingRepository {
    fun getAllCampaigns(): Single<Campaigns>
}
