package com.andriikovalchuk.westwingtest01.repository.campaigns.model

import com.google.gson.annotations.SerializedName

data class MetadataResponse(

	@field:SerializedName("data")
	val data: List<DataItemResponse?>? = null,

	@field:SerializedName("count")
	val count: Int? = null
)