package com.andriikovalchuk.westwingtest01.repository.campaigns.model

import com.google.gson.annotations.SerializedName

data class DataItemResponse(

	@field:SerializedName("image")
	val image: ImageResponse? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("url_key")
	val urlKey: String? = null
)