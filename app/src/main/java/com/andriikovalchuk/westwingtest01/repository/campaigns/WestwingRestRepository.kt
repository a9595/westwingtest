package com.andriikovalchuk.westwingtest01.repository.campaigns

import com.andriikovalchuk.westwingtest01.domain.model.Campaigns
import com.andriikovalchuk.westwingtest01.domain.repositories.WestwingRepository
import io.reactivex.Single

class WestwingRestRepository(
    private val service: WestwingService
) : WestwingRepository {

    override fun getAllCampaigns(): Single<Campaigns> {
        return service.getAllCampaigns().map { Campaigns(it) }
    }
}