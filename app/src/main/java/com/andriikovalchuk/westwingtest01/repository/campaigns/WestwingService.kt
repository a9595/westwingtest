package com.andriikovalchuk.westwingtest01.repository.campaigns

import com.andriikovalchuk.westwingtest01.repository.campaigns.model.CampaignsResponseEntity
import io.reactivex.Single
import retrofit2.http.GET

interface WestwingService {

    @GET("cms/test/data.json")
    fun getAllCampaigns(): Single<CampaignsResponseEntity>
}