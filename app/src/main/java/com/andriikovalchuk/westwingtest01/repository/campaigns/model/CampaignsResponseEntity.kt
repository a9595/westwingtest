package com.andriikovalchuk.westwingtest01.repository.campaigns.model

import com.google.gson.annotations.SerializedName

data class CampaignsResponseEntity(

    @field:SerializedName("metadata")
    val metadata: MetadataResponse? = null,

    @field:SerializedName("success")
    val success: Boolean? = null,

    @field:SerializedName("messages")
    val messages: MessagesResponse? = null
) {
}