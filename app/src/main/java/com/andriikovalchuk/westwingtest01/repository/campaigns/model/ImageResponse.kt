package com.andriikovalchuk.westwingtest01.repository.campaigns.model

import com.google.gson.annotations.SerializedName

data class ImageResponse(

	@field:SerializedName("url")
	val url: String? = null
)