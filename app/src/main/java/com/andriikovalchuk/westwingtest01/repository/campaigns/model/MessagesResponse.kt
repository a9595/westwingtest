package com.andriikovalchuk.westwingtest01.repository.campaigns.model

import com.google.gson.annotations.SerializedName

data class MessagesResponse(

	@field:SerializedName("success")
	val success: List<String?>? = null
)